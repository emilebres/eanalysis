from scipy import stats

class Test():
	"""statistical test"""
	def __init__(self, label, function):
		self.label = label
		self.function = function

	def main(self, data):
		try:
			self.res = self.run(data)
			return self.res
		except:
			self.res = (None, None)
			return self.res

	def __str__(self):
		ret = [self.label]
		ret.append('Result: {}'.format(self.res))
		return '\n'.join(ret)

	def run(self, data):
		args = data.values
		return self.function(args)

class FisherExactTest(Test):
	"""Fisher's exact test for independence"""

	def __init__(self):
		self.label = "Fisher's exact test for independence"
		self.function = stats.fisher_exact

class ChiSquareTest(Test):
	"""Chi square test for independence"""

	def __init__(self):
		self.label = 'Chi square test for independence'
		self.function = stats.chi2_contingency

	def run(self, data):
		data = data.replace(to_replace=0, value = 0.01)
		args = data.values
		return self.function(args)

class StudentTTest(Test):
	"""Student's unpaired t-test"""
	def __init__(self):
		self.label = "Student's unpaired t-test"
		self.function = stats.ttest_ind

	def run(self, data):
		args = list(data.values())
		return self.function(*args)

class WelchTTest(Test):
	"""Welch's unpaired t-test"""
	def __init__(self):
		self.label = "Welch's unpaired t-test"
		self.function = stats.ttest_ind

	def run(self, data):
		args = list(data.values())
		return self.function(*args, equal_var=False)

class KruskalWallisTest(Test):
	"""Kruskal-Wallis test"""
	def __init__(self, nonSimilarDev=False):
		self.label = "Kruskal-Wallis test"
		self.function = stats.kruskal
		self.nonSimilarDev = nonSimilarDev

	def run(self, data):
		args = list(data.values())
		return self.function(*args)

class WilcoxonSignedRankTest(Test):
	"""Wilcoxon signed rank test"""
	def __init__(self, nonSimilarDev = False):
		self.label = "Wilcoxon signed rank test"
		self.function = stats.wilcoxon
		self.nonSimilarDev = nonSimilarDev

	def run(self, data):
		args = list(data.values())
		return self.function(*args)

class OneWayAnovaTest(Test):
	"""One-way Anova test"""
	def __init__(self,  nonHomogenousVar = False):
		self.label = 'one-way Anova test'
		self.function = stats.f_oneway
		self.nonHomogenousVar = nonHomogenousVar

	def run(self, data):
		args = list(data.values())
		return self.function(*args)

class LinearRegressionTest(Test):
	"""Linear regression test"""
	def __init__(self):
		self.label = 'linear regression test'
		self.function = stats.linregress

	def run(self, data):
		return self.function(data)
