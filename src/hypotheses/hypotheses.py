import pandas as pd
from scipy import stats
import math

class Hypothesis():
    """A statistical hypothesis"""
    def __init__(self):
        self.key = 'empty'
        self.label = 'empty hypothesis'
        self.data = None

    def __str__(self):
        ret = []
        ret.append('label: {}'.format(self.label))
        if self.res:
            ret.append('hypothesis TRUE')
        else:
            ret.append('hypothesis FALSE')
        return "\n".join(ret)

    def check(self):
        pass

class SampleSizeGreaterThan(Hypothesis):
    """checks size of sample"""


    def __init__(self, size, data):
        self.data = data
        self.size = size
        self.label = 'The sample size is greater than {}'.format(size)

    def check(self):
        sample_size = self.data.sum().sum()
        self.res = (sample_size>self.size)
        return self.res

    def renderHTML(self):
        res="<p>{}: ".format(self.label)
        res+="<b>{}</b> (actual sample size: {})</p>".format('verified' if self.res else 'not verified', self.data.sum().sum())
        return res

class TwoTwoTable(Hypothesis):
    """checks if the contingency table is of shape (2, 2)"""
    def __init__(self, data):
        self.data = data
        self.label = "Both variables are binary"

    def check(self):
        self.res = self.data.shape == (2, 2)
        return self.res

    def renderHTML(self):
        res="<p>{}: ".format(self.label)
        res+="<b>{}</b> (the independent variable has {} groups and the dependent variable {} groups)</p>".format('verified' if self.res else 'not verified', self.data.shape[0], self.data.shape[1])
        return res


class Normality(Hypothesis):
    """checks normal distribution of groups"""

    def __init__(self, data, p = 0.05):
        self.p = p
        self.data = data
        self.label = 'The data is normally distributed in each group'
        self.value = {}

    def check(self):
        # values = list(self.data[self.indep_var].unique())
        # group_data={}
        # for v in values:
        #     group_data[v]= self.data[self.dep_var].loc[self.data[self.indep_var] == v]
        # Checking for normality
        # if sample size is small (n<500) Do a Shapiro Wilk test
        # Do skewness and curtosis measurement, compute z-score for both
        # hypo['skewness is normal']={}
        # hypo['kurtosis is normal']={}
        groups = self.data.keys()
        if max([len(x) for x in self.data.values()])<500:
            self.label += ' (Shapiro-Wilk test)'
            for g in groups:
                try:
                    self.value[g]=stats.shapiro(self.data[g])
                except:
                    self.value[g]=(None, None)
        else:
            self.label += " (D' Agostino-Pearson test)"
            for g in groups:
                try:
                    self.value[g]=stats.shapiro(self.data[g])
                except:
                    self.value[g]=(None, None)
        self.value = pd.DataFrame.from_dict(self.value, orient='index')
        self.value.columns = ['statistic', 'p-value']

        self.res = min([x[1][1] for x in self.value.items()])<self.p
        return self.res

    def renderHTML(self):
        res="<p>{}: ".format(self.label)
        res+="<b>{}</b></p>".format('verified' if self.res else 'not verified')
        res += self.value.to_html()
        return res

class VarianceHomogeneity(Hypothesis):
    """check if the variances are homogenous between groups"""

    def __init__(self, data, p = 0.05):
        self.data = data
        self.p = p
        self.label = 'The variances are homogenous between groups'

    def check(self):
        self.value = stats.bartlett(*self.data.values())
        self.res = self.value[1]>self.p
        return self.res

    def renderHTML(self):
        res="<p>{}: ".format(self.label)
        res+="<b>{}</b></p>".format('verified' if self.res else 'not verified')
        res += '<p><b>statistic value</b>: {}</p>'.format(self.value[0])
        res += '<p><b>p-value</b>: {}</p>'.format(self.value[1])
        return res

class SameDeviation(Hypothesis):
    """checks if groups have the same deviation (skewness and kurtosis)"""

    def __init__(self, data, p = 0.05):
        self.data = data
        self.label = 'The groups have the same deviation (skewness and kurtosis)'
        self.value = pd.DataFrame(columns=['skewness', 'kurtosis'], index=self.data.keys())

    def check(self):
        for g in self.data.keys():
            self.value.ix[[g], ['skewness']] = stats.kurtosis(self.data[g])
            self.value.ix[[g], ['kurtosis']] = stats.skew(self.data[g])
        res_skew = all(item >= 0 for item in list(self.value['skewness'])) or all(item < 0 for item in list(self.value['skewness']))
        res_kurto = all(item >= 0 for item in list(self.value['kurtosis'])) or all(item < 0 for item in list(self.value['kurtosis']))
        self.res = res_skew and res_kurto
        return self.res

    def renderHTML(self):
        res="<p>{}: ".format(self.label)
        res+="<b>{}</b></p>".format('verified' if self.res else 'not verified')
        res += self.value.to_html()
        if self.res:
            res += '<p>The different groups deviate in the same direction (for skewness and kurtosis).<p>'
        else:
            res += '<p>The different groups do NOT deviate in the same direction (for skewness and kurtosis).<p>'
        return res





