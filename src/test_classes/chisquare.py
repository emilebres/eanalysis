import pandas as pd
import numpy as np
import itertools
import math
from collections import OrderedDict
import networkx as nx

from src.test_classes.test_class import TestClass
from src.tests.tests import *
from src.hypotheses.hypotheses import *


class ChiSquareTestClass(TestClass):
    """class of chi square tests"""
    def __init__(self, raw_data, variables, p = 0.05):
        self.p = p
        self.raw_data = raw_data
        self.variables = variables
        self.label = 'chi square tests'
        self.hypotheses = []
        self.tests = [FisherExactTest, ChiSquareTest]

    def process_data(self):
        indep_var = self.variables['independent'][0]
        dep_var = self.variables['dependent'][0]
        # confounding_var = self.variables['confounding'][0]
        indep_serie = self.raw_data[indep_var]
        dep_serie = self.raw_data[dep_var]
        return pd.crosstab(indep_serie,dep_serie)

    def select_test(self):
        sizeHyp = SampleSizeGreaterThan(1000, self.data)
        twotwoHyp = TwoTwoTable(self.data)
        self.hypotheses =[sizeHyp, twotwoHyp]
        for h in self.hypotheses:
            h.check()
        if not twotwoHyp.res:
            return ChiSquareTest()
        else:
            if sizeHyp.res:
                return ChiSquareTest()
            else:
                return FisherExactTest()


    def post_test(self):
        res = OrderedDict()
        # Cramer V: strength of association
        cramer = math.sqrt((self.res[0]/len(self.raw_data))/(min(self.data.shape)-1))
        res['Strength of association'] = '<br>After assessing statistical significance, we measure the strength of association using Cramer\'s V.</br> V = {}, which corresponds to a <em>{}</em> association.'.format(cramer, 'weak' if cramer<0.2 else ('medium' if cramer < 0.4 else 'strong'))
        # chi2 test for all pairs
        pairs = list(itertools.combinations(list(self.data.index.values), 2))
        if len(pairs)>1:
            pairs_dict = OrderedDict()
            not_assoc = []
            for p in pairs:
                pairs_df = self.raw_data.ix[self.raw_data[self.variables['independent'][0]].isin(p),self.variables['dependent']+self.variables['independent']]
                chi2 = ChiSquareTestClass(pairs_df, self.variables)
                chi2.data = self.data.loc[list(p)]
                chi2.test = chi2.select_test()
                pairs_dict[p] = chi2.test.run(chi2.data)[1]
                if pairs_dict[p]>self.p/len(pairs):  # Bonferroni correction
                    not_assoc.append(p)
                    pairs_dict[p] = [pairs_dict[p],'No']
                else:
                    pairs_dict[p] = [pairs_dict[p],'Yes']
            g = nx.Graph()
            g.add_edges_from(not_assoc)
            cliques = list(nx.find_cliques(g))
            pairs_df = pd.DataFrame.from_dict(pairs_dict,orient='index')
            pairs_df.columns = ['p value', 'Significant association']
            bonf_sign = lambda x: '<span class="bonf_sign">{: .3e}</span>'.format(x) if x<self.p/len(pairs) else '{: .3e}'.format(x)
            html_pairs_df = pairs_df.to_html(formatters={'p value': bonf_sign}, escape=False)
            res['Pairs'] = '''<p>In order to see if there are significant associations within groups, we conduct a chi square test for each pair.
            The pairs for which we find a significant association are colored in red.
            <br><div class="constrainer">{}</div><br>After applying the Bonferroni correction, we can assess the groups which are not statistically different:<ul>'''.format(html_pairs_df)
            for c in cliques:
                res['Pairs'] +='<li>{}</li>'.format(', '.join(c))
            res['Pairs'] +='</ul>'
        # Comparing residuals
        residuals = self.data - self.res[3]
        row_margins = self.data.sum(axis=1)
        col_margins = self.data.sum()
        std_residuals = residuals/np.vectorize(math.sqrt)(self.res[3])
        total = col_margins.sum()
        col_factors = 1/np.vectorize(math.sqrt)(1-(col_margins/total))
        row_factors = 1/np.vectorize(math.sqrt)(1-(row_margins/total))
        adj_std_residuals = std_residuals.mul(col_factors).transpose().mul(row_factors).transpose()
        res_sign = lambda x: '<span class="res_sign">{: .3e}</span>'.format(x) if abs(x) > 2 else '{: .3e}'.format(x)
        html_res_df = adj_std_residuals.to_html(formatters=[res_sign]*len(adj_std_residuals.columns), escape=False)
        res['Adjusted standard residuals'] ='''
            <p>In order to assess which cells contributed the most to the chi square value, we calculate the adjusted standard residuals.
            <br><div class="constrainer" id=\"res\">{}</div></p>
            <p>An adjusted standardized residual having absolute value
            greater than 2 indicates lack of fit of the null hypothesis in that cell (Agrosti, 2007).
            We mark these residuals in red.</p>'''.format(html_res_df)
        return res

    def testResHTML(self):
        res = '<p>Contigency table</p>'
        res += '<div class="constrainer">{}</div>'.format(self.data.to_html())
        # res += '<hr>'
        res += '<p>The results of the test are the following:</p>'
        res += '<p><b>chi square</b>: {: .3e}</p>'.format(self.res[0])
        res += '<p><b>p value</b>: {: .3e}</p>'.format(self.res[1])
        # res += '<hr>'
        association = self.res[1] < self.p
        res += '<p>Interpretation</p>'
        res += '<p>p {} {}. There is {} statistically significant association between <b>{}</b> and <b>{}</b>.'.format('<' if association else '>', self.p ,'a' if association else '<b>no</b>', self.variables['independent'][0], self.variables['dependent'][0])
        return res

    # def visualisation(self):
    #     vizDataFrame = self.raw_data.ix[:,self.variables['dependent']+self.variables['independent']]
    #     return charts.Bar(vizDataFrame, legend = 'top_right', values = self.variables['dependent'][0], label = self.variables['independent'][0], group = self.variables['dependent'][0], agg = 'count')