import pandas as pd
import numpy as np
from scipy import stats
import itertools
import math
import networkx as nx
from collections import OrderedDict

from src.tests.tests import *
from src.hypotheses.hypotheses import *

class TestClass():
    """class of statistical tests"""
    def __init__(self):
        self.label = 'empty test class'
        self.data = None
        self.hypotheses = []
        self.tests = []
        self.pot_confounders = []

    # for debugging
    def __repr__(self):
        return 'TestClass("{}")'.format(self.label)

    # for reporting
    def __str__(self):
        return self.label

    def main(self, simple = False):
        if self.label == 'empty test class':
            return
         # Data preprocessing
        self.data = self.process_data()

        self.test = self.select_test()

        self.res = self.test.main(data = self.data)

        # Run posthoc tests if rejection of null hypothesis
        if (self.res[1] < self.p) and not simple:
            self.posthoc_res = self.post_test()
        else:
            self.posthoc_res = {}
        return self.res

    def process_data(self):
        pass

    def select_test(self):
        pass


    def post_test(self):
        pass

    def selectionHTML(self):
        n = len(self.hypotheses)
        res = "<p>The <b>{}</b> test class contains the following tests:</p>".format(self.label)
        res +='<ul>'
        for t in self.tests:
            res +='<li>'
            res += t().label
            res +='</li>'
        res +='</ul>'
        res+='<p>These tests have the same purpose (to assess whether there is an association between the dependent and independent variables).<br/>However such tests are only valid under certain hypotheses which vary from one test to another.<br/> The system will verify all the necessary hypotheses in order to select the appropriate test for the data.</p>'
        res += "<p>There {} {} {} to consider.</p>".format('is' if n == 1 else 'are', n, 'hypothesis' if n == 1 else 'hypotheses')
        res +='<ul>'
        for h in self.hypotheses:
            res +='<li>'
            res += h.renderHTML()
            res +='</li>'
        res +='</ul>'
        res += "<p>In consequence, the selected test is <b>{}</b>.</p>".format(self.test.label)
        return res

    def visualisation(self):
        return 'No visualisation available.'

    def testResHTML(self):
        res = '<p>The results of the test are the following:</p>'
        res += '<p><b>statistic value</b>: {: .3e}</p>'.format(self.res[0])
        res += '<p><b>p value</b>: {: .3e}</p>'.format(self.res[1])
        return res

    def posthocHTML(self):
        if len(self.posthoc_res.items()) == 0:
            res = '<p>No post hoc tests to conduct.</p>'
        else:
            res = '<p>To obtain more information, we conducted some tests after the main test.</p>'
            res +='<ul>'
            for (k,v) in self.posthoc_res.items():
                res +='<li>'
                res += '<b>{}</b>{}'.format(k, v)
                res +='</li>'
            res +='</ul>'
        return res

    def is_categorical(self, var):
        return 'object' == self.raw_data[var].dtypes.name

    def analyse_confounding(var):
        # stratification plus Cochran Mantel
        # if numerical variables, put in 4 bins by using medians
        pass


    def find_confounding(self):
        variables = [x for x in self.raw_data.columns if not [x] in self.variables.values()]
        pot_conf = []
        dep = OrderedDict()
        indep = OrderedDict()
        n = len(variables)
        for v in variables:
            res = self.is_potential_confounding(v)
            dep[v] = res[0]
            if res[0]['p value'] < self.p/n: # Bonferroni correction
                dep[v]['Association']='Yes'
            else:
                dep[v]['Association']='No'
            indep[v] = res[1]
            if res[1]['p value'] < self.p/n: # Bonferroni correction
                indep[v]['Association']='Yes'
            else:
                indep[v]['Association']='No'
            if (dep[v]['Association'] is 'Yes') & (indep[v]['Association'] is 'Yes'):
                pot_conf.append(v)
        dep_df = pd.DataFrame.from_dict(dep, orient='index')
        indep_df = pd.DataFrame.from_dict(indep, orient='index')
        res_df = pd.concat([dep_df,indep_df], keys = [self.variables['dependent'][0], self.variables['independent'][0]], axis = 1)
        res_df.loc[pot_conf,'Potential confounder'] = 'Yes'
        res_df.loc[[x for x in variables if not x in pot_conf], 'Potential confounder'] = 'No'
        self.pot_confounders = pot_conf
        return res_df

    def is_potential_confounding(self, var):
        dep_res = self.are_associated([var], self.variables['dependent'])
        indep_res = self.are_associated([var], self.variables['independent'])
        # return line of HTML table
        return dep_res, indep_res

    def are_associated(self, var1, var2):
        from src.main import Analysis
        analysis = Analysis(self.raw_data, self.p)
        if not self.is_categorical(var1[0]):
            analysis.variables = {'independent':var2, 'dependent': var1}
        else:
            analysis.variables = {'independent':var1, 'dependent': var2}
        res = analysis.main(simple = True)
        return OrderedDict([('Test', analysis.test_class.test.label), ('Test statistic', res[0]), ('p value', res[1])])

    def stratified_analysis(self, conf_var):
        from src.main import Analysis
        conf_var_org = conf_var
        res, html = OrderedDict(), OrderedDict()
        # if the data is numerical, discretize data in 4 bins
        is_cat = self.is_categorical(conf_var)
        if not is_cat:
            cat_serie = pd.qcut(self.raw_data[conf_var],4)
            conf_var = conf_var + '_cat_'
            cat_serie.name = conf_var
            self.raw_data = pd.concat([self.raw_data, cat_serie], axis =1)
        levels = self.raw_data[conf_var].unique()
        for lvl in levels:
            analysis = Analysis(self.raw_data[self.raw_data[conf_var]==lvl], self.p)
            analysis.variables = self.variables
            res[lvl] = analysis.main(simple = True)
            html[lvl] = '''<p><b>Group {} of {}</b></p>
            {}
            '''.format(lvl, conf_var_org, analysis.test_class.testResHTML())
        if not is_cat: del self.raw_data[conf_var]
        # if the stratified tables are 2x2, do a pooling analysis (Cochran Mantel Haenszel chi square)
        # if self.data.shape == (2,2):
        #     pass
        return res, html

    def calculate_odd_ratios(table):
        pass

    def two_two_odd_ratios(table):
        c = np.asarray(table, dtype=np.int64)  # int32 is not enough for the algorithm

        if np.any(c < 0):
            raise ValueError("All values in `table` must be nonnegative.")

        if 0 in c.sum(axis=0) or 0 in c.sum(axis=1):
            # If both values in a row or column are zero, the p-value is 1 and
            # the odds ratio is NaN.
            return np.nan, 1.0

        if c[1,0] > 0 and c[0,1] > 0:
            return c[0,0] * c[1,1] / float(c[1,0] * c[0,1])
        else:
            return np.inf