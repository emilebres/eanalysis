import pandas as pd
import numpy as np
import itertools
import math
from collections import OrderedDict
import networkx as nx

from src.test_classes.test_class import TestClass
from src.tests.tests import *
from src.hypotheses.hypotheses import *

class OneWayAnovaTestClass(TestClass):
    """class of one-way anova tests"""
    def __init__(self, raw_data, variables, p = 0.05):
        self.raw_data = raw_data
        self.variables = variables
        self.p = p
        self.label = 'one-way anova tests'
        self.tests = [OneWayAnovaTest, KruskalWallisTest]
        self.hypotheses = []

    def process_data(self):
        indep_var = self.variables['independent'][0]
        dep_var = self.variables['dependent'][0]
        values = list(self.raw_data[indep_var].unique())
        data_dict={}
        for v in values:
            data_dict[v]= self.raw_data[dep_var].loc[self.raw_data[indep_var] == v]
        return data_dict

    def select_test(self):
        normal = Normality(self.data)
        deviation = SameDeviation(self.data)
        variance = VarianceHomogeneity(self.data)
        self.hypotheses = [normal, deviation, variance]
        for h in self.hypotheses:
            h.check()
        if normal.res:
            if variance.res:
                return OneWayAnovaTest()
            else:
                return OneWayAnovaTest(nonHomogenousVar = True)
        elif deviation.res:
            return KruskalWallisTest()
        else:
            return KruskalWallisTest(nonSimilarDev = True)

    def testResHTML(self):
        res = '<p>The results of the test are the following:</p>'
        res += '<p><b>statistic value</b>: {: .3e}</p>'.format(self.res[0])
        res += '<p><b>p value</b>: {: .3e}</p>'.format(self.res[1])
        res += '<hr>'
        association = self.res[1] < self.p
        res += '<p><b>Interpretation</b></p>'
        res += '<p>p {} {}. There is {} statistically significant association between {} and {}.'.format('<' if association else '>', self.p ,'a' if association else '<b>no</b>', self.variables['dependent'][0], self.variables['independent'][0])
        return res

    def post_test(self):
        res = {}
        # Cramer V: strength of association
        # cramer = math.sqrt((self.res[0]/len(self.raw_data))/(min(self.data.shape)-1))
        # res['Strength of association'] = '<br>After assessing statistical significance, we measure the strength of association using Cramer\'s V.</br> V = {}, which corresponds to a {} association.'.format(cramer, 'weak' if cramer<0.2 else ('medium' if cramer < 0.4 else 'strong'))
        # t test for all pairs
        pairs = list(itertools.combinations(list(self.data.keys()), 2))
        if len(pairs)>1:
            pairs_dict = {}
            not_assoc = []
            for p in pairs:
                data_dict = {x:self.data[x] for x in p}
                pairs_dict[p] = KruskalWallisTest().main(data_dict).pvalue
                if pairs_dict[p]>self.p/len(pairs):  # Bonferroni correction
                    not_assoc.append(p)
                    pairs_dict[p] = [pairs_dict[p],'No']
                else:
                    pairs_dict[p] = [pairs_dict[p],'Yes']
            g = nx.Graph()
            g.add_edges_from(not_assoc)
            cliques = list(nx.find_cliques(g))
            pairs_df = pd.DataFrame.from_dict(pairs_dict,orient='index')
            pairs_df.columns = ['p value', 'Significant association']
            bonf_sign = lambda x: '<span class="bonf_sign">{: .3e}</span>'.format(x) if x<self.p/len(pairs) else '{: .3e}'.format(x)
            html_pairs_df = pairs_df.to_html(formatters={'p value': bonf_sign}, escape=False)
            res['Pairs'] = '''<p>In order to see if there are significant associations within groups, we conduct a chi square test for each pair.
            The pairs for which we find a significant association are colored in red.
            <br><div class="constrainer">{}</div><br>After applying the Bonferroni correction, we can assess the groups which are not statistically different:<ul>'''.format(html_pairs_df)
            for c in cliques:
                res['Pairs'] +='<li>{}</li>'.format(c)
            res['Pairs'] +='</ul>'
        return res

    # def visualisation(self):
    #     vizDataFrame = self.raw_data.ix[:,self.variables['dependent']+self.variables['independent']]
    #     return charts.Histogram(vizDataFrame, legend = 'top_right', values = self.variables['dependent'][0], color=self.variables['independent'][0])
    #     # return charts.Bar(vizDataFrame, values = self.variables['dependent'][0], label = self.variables['dependent'][0], agg = 'count')
