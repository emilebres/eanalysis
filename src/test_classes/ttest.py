import pandas as pd
import numpy as np
import itertools
import math
from collections import OrderedDict

from src.test_classes.test_class import TestClass
from src.tests.tests import *
from src.hypotheses.hypotheses import *

class TTestClass(TestClass):
    """class of t-tests"""
    def __init__(self, raw_data, variables, p = 0.05):
        self.raw_data = raw_data
        self.variables = variables
        self.p = p
        self.label = 't tests'
        self.tests = [StudentTTest, WelchTTest, KruskalWallisTest]
        self.hypotheses = []

    def process_data(self):
        indep_var = self.variables['independent'][0]
        dep_var = self.variables['dependent'][0]
        values = list(self.raw_data[indep_var].unique())
        data_dict={}
        for v in values:
            data_dict[v]= self.raw_data[dep_var].loc[self.raw_data[indep_var] == v]
        return data_dict

    def select_test(self):
        normal = Normality(self.data)
        deviation = SameDeviation(self.data)
        variance = VarianceHomogeneity(self.data)
        self.hypotheses = [normal, deviation, variance]
        for h in self.hypotheses:
            h.check()
        if normal.res:
            if variance.res:
                return StudentTTest()
            else:
                return WelchTTest()
        elif deviation.res:
            return KruskalWallisTest()
        else:
            return KruskalWallisTest(nonSimilarDev = True)

    def post_test(self):
        return {}


    # def visualisation(self):
    #         vizDataFrame = self.raw_data.ix[:,self.variables['dependent']+self.variables['independent']]
    #         return charts.Histogram(vizDataFrame, legend = 'top_right', values = self.variables['dependent'][0], color=self.variables['independent'][0])
            # return charts.Bar(vizDataFrame, values = self.variables['dependent'][0], label = self.variables['dependent'][0], agg = 'count')

