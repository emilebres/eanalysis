import pandas as pd
import numpy as np
import itertools
import math
from collections import OrderedDict

from src.test_classes.test_class import TestClass
from src.tests.tests import *
from src.hypotheses.hypotheses import *

class LinearRegressionTestClass(TestClass):
    """class of regression slope tests"""
    def __init__(self, raw_data, variables, p = 0.05):
        self.raw_data = raw_data
        self.variables = variables
        self.p = p
        self.label = 'linear regression tests'
        self.tests = [LinearRegressionTest]
        self.hypotheses = []

    def process_data(self):
        return self.raw_data[[self.variables['dependent'][0], self.variables['independent'][0]]]

    def select_test(self):
        return LinearRegressionTest()




