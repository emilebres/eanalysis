import pandas as pd
# local module
from src.test_classes.test_class import TestClass
from src.test_classes.chisquare import ChiSquareTestClass
from src.test_classes.ttest import TTestClass
from src.test_classes.anova import OneWayAnovaTestClass
from src.test_classes.linear_regression import LinearRegressionTestClass

class Analysis(object):
# There are hypotheses that can be tested within the data (normality, homodaesticity) and other who cannot, who are relative to the experiment design. Those should be mentioned to the user and perhaps ask the user to confirm.

    def __init__(self, raw_data = None, p = 0.05):
        self.variables = {'dependent':[], 'independent':[]}
        self.raw_data = raw_data
        self.p = p

    def main(self, simple = False):
        self.test_class = self.select_test_class()
        return self.test_class.main(simple)

    def import_data(self, data_file, sep = ',', header=0):
        self.raw_data = pd.read_csv(data_file, sep=sep, header=header)

    def is_categorical(self, var):
        return 'object' == self.raw_data[var].dtypes.name

    def construct_table_var(self):
        html_table = "<table style =\"width:40%\">"
        # html_table = "<table id=\"svars\">"
        if len(self.variables['dependent']) > 0:
            for v in self.variables['dependent']:
                html_table += "<tr><td><b>Dependent variable<b></td><td>{}</td><td><i>{}</i></td></tr>".format(v, 'categorical' if self.is_categorical(v) else 'numerical')
        if len(self.variables['independent']) > 0:
            v = self.variables['independent'][0]
            html_table += "<tr><td rowspan = \"{}\"><b>Independent variables<b></td><td>{}</td><td><i>{}</i></td></tr>".format(len(self.variables['independent']), v, 'categorical' if self.is_categorical(v) else 'numerical')
            for v in self.variables['independent'][1:]:
                html_table += "<tr><td>{}</td><td><i>{}</i></td></tr>".format(v, 'categorical' if self.is_categorical(v) else 'numerical')
        return html_table

    def select_test_class(self):
        dep = list(self.variables['dependent'])
        ind = list(self.variables['independent'])

        question = 0

        test_class = TestClass()
        if question == 0:
            if len(dep) == 1 and self.is_categorical(dep[0]) and len(ind) == 1 and self.is_categorical(ind[0]):
                # test_class = 'chi2'
                test_class = ChiSquareTestClass(self.raw_data, self.variables, self.p)
            elif len(dep) == 1 and not self.is_categorical(dep[0]) and len(ind) == 1 and self.is_categorical(ind[0]) and len(self.raw_data[ind[0]].unique()) == 2:
                # test_class = 't-test_class'
                test_class = TTestClass(self.raw_data, self.variables, self.p)
            elif len(dep) == 1 and not self.is_categorical(dep[0]) and len(ind) == 1 and self.is_categorical(ind[0]) and not len(self.raw_data[ind[0]].unique()) == 2:
                test_class = OneWayAnovaTestClass(self.raw_data, self.variables, self.p)
            elif len(dep) == 1 and not self.is_categorical(dep[0]) and len(ind) == 1 and not self.is_categorical(ind[0]):
                test_class = LinearRegressionTestClass(self.raw_data, self.variables, self.p)
            # elif len(dep) == 1 and not self.is_categorical(dep[0]) and len(ind)>=2 and (self.is_categorical(x) for x in ind):
            #     test_class = 'two-way anova'
            # elif len(dep) >= 2 and not ('category' in [x[1] for x in dep]) and len(ind) == 1 and self.is_categorical(ind[0]):
            #     test_class = 'one-way manova'
            # elif len(dep) >= 2 and not ('category' in [x[1] for x in dep]) and len(ind) >=2 and ['category' for x in dep] == [x[1] for x in dep]:
            #     test_class = 'two-way manova'
            # if len(dep) == 1 and not (dep[0][1] == 'category') and len(ind) == 1 and not (ind[0][1] == 'category'):
            #     test_class = 'bivariate correlation'
            # elif len(dep) == 1 and not (dep[0][1] == 'category') and len(ind) >= 2 and not ('category' in [x[1] for x in dep]):
            #     test_class = 'multiple regression'
            # elif len(dep) >= 2 and not ('category' in [x[1] for x in dep]) and len(ind) >= 2 and not ('category' in [x[1] for x in ind]):
            #     test_class = 'path analysis'
            # if len(dep) == 1 and len(self.raw_data[dep[0][0]].unique()) == 2 and len(ind) >= 1 and ((not 'category' in [x[1] for x in ind]) or (['category' for x in dep] == [x[1] for x in dep])):
            #     test_class = 'logistic regression'
        return test_class

