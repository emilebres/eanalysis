eAnalysis is a tool to help non-trained users do statistical analysis. 

Its features are:

* data exploration
* automated test selection
* test hypotheses assessment
* post hoc analysis
* confounders analysis

eAnalysis is built on Jupyter. You can try a live version of eAnalysis [here](http://128.199.231.116:8000/notebooks/easy_analysis.ipynb).